import React, { Component } from 'react';
import Result from './Result';
import './Calculator.css';

class Calculator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      numbers: this.props.initialNumbers,
      method: this.props.initialMethod,
      displayControls: (this.props.displayControls === true)
    }
  }

  toggleControls = () => {
    this.setState((prevState) => {
      return({ displayControls: !!! prevState.displayControls });
    });
  }

  updateMethod = (val) => {
    if (val==="nil") {
      this.setState({ method: null });
    } else {
      this.setState({ method: val });
    }
  }

  redVotes = () => (this.state.numbers[0] + this.state.numbers[1]);
  noneVotes = () => (this.state.numbers[2] + this.state.numbers[3]);
  blueVotes = () => (this.state.numbers[4] + this.state.numbers[5]);

  updateNumber = (n, val) => {
    this.setState( (prevState) => {
      let new_num = prevState.numbers;
      new_num[n] = parseInt(val);
      return({ numbers: new_num });
    });
  }

  input = (label, name, num) => {
    return(
      <div className="control">
        <div className="label">{ label }</div>
        <input name={name} value={ this.state.numbers[num] } onChange={ (e) => this.updateNumber(num, e.target.value) } type="number" min="0" />
      </div>
    );
  }

  render() {
    let methods = [
      ["simpelt", "Simpelt flertall"],
      ["landsstyret", "Landsstyrets metode"],
      ["runoff", "Run-off (eliminere svakeste)"],
      ["alternativt", "Alternativ votering"],
      ["condorcet", "Condorcet-metode"]
    ];

    const method_select = methods.map((elmt) => {
      return(
        <button className="methodButton" key={ elmt[0] } onClick={ () => this.updateMethod(elmt[0]) }>
          { elmt[1] }
        </button>
      );
    });

    const controls = <div className="controls">
        <p>Flytt stemmer mellom de seks mulige preferansene – to ulike andreønsker for hvert av de tre førsteønskene.</p>

        <div className="votes-panel">
          <div className="primary red">
            <div className="label">Førsteønske: venstresiden</div>
            <div className="number">{ this.redVotes() }</div>
            <div className="secondary-label">Andreønske</div>
            <div className="secondary">
              { this.input('Opposisjon', 'rn', 0) }
              <div>+</div>
              { this.input('Høyresiden', 'rb', 1) }
            </div>
          </div>
          <div className="primary none">
            <div className="label">Førsteønske: opposisjon</div>
            <div className="number">{ this.noneVotes() }</div>
            <div className="secondary-label">Andreønske</div>
            <div className="secondary">
              { this.input('Venstresiden', 'nr', 2) }
              <div>+</div>
              { this.input('Høyresiden', 'nb', 3) }
            </div>
          </div>
          <div className="primary blue">
            <div className="label">Førsteønske: høyresiden</div>
            <div className="number">{ this.blueVotes() }</div>
            <div className="secondary-label">Andreønske</div>
            <div className="secondary">
              { this.input('Venstresiden', 'br', 4) }
              <div>+</div>
              { this.input('Opposisjon', 'bn', 5) }
            </div>
          </div>
        </div>

        <p><b>Velg metode:</b></p>
        <div className="methodButtons">
            { method_select }
          </div>
        </div>;

    const toggleControls = <button className="toggleControls" onClick={ this.toggleControls }>
      { this.state.displayControls ? "Skjul innstillinger" : "Vis innstillinger" }
    </button>;

    return(
      <div className="Calculator">
        { this.state.displayControls ? controls : null }
        { this.state.method ? <Result method={ this.state.method } numbers={ this.state.numbers } /> : null }

        { this.props.displayControls !== true ? toggleControls : null }
      </div>
    );
  }
}

export default Calculator;
