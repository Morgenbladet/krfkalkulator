import React, { Component } from 'react';
import './Vote.css';

class Vote extends Component {

  render() {
    const alternatives = this.props.alternatives.map((alt, i) => <li key={ "alt-" + i  }>{ alt[0] }: { alt[1] } stemmer</li>);
    return(
      <li className="Vote">
        <span className="description">{ this.props.description }</span>
        <ul className="alternatives"> { alternatives }</ul>
        <span className="result">Resultat: { this.props.result }</span>
      </li>
    );
  }
}

export default Vote;
