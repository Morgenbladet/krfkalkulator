import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Result from './Result';
import Calculator from './Calculator';

let results = document.getElementsByClassName("krf-result");

let default_votes = [35, 30, 30, 30, 30, 35];

for(let i = 0; i < results.length; i++) {
  let elmt = results[i];
  let data = elmt.dataset.voters.split(" ").map((el) => parseInt(el));
  ReactDOM.render(<Calculator initialMethod={ elmt.dataset.votemethod } initialNumbers={ data } displayControls={ false }/>, elmt);
}


ReactDOM.render(<Calculator initialMethod="simpelt" displayControls={ true } initialNumbers={ default_votes }/>, document.getElementById("krf-calculator"));

