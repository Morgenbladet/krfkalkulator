import React, { Component } from 'react';
import './Result.css';
import Vote from './Vote.js';
import isEqual from 'lodash-es';

class Result extends Component {

  constructor(props) {
    super(props);
    this.state = this.initState();
  }

  initState = () => {
    let method = null;
    let methodname;
    if (this.props.method === 'landsstyret') {
      method = this.landsstyret;
      methodname = "Landsstyrets foretrukne metode";
    } else if (this.props.method === 'runoff') {
      method = this.runoff;
      methodname = "Runoff (eliminere svakeste)";
    } else if (this.props.method === 'simpelt') {
      method = this.simpelt;
      methodname = "Simpelt flertall";
    } else if (this.props.method === 'alternativt') {
      method = this.alternativt;
      methodname = "Alternativ votering";
    } else if (this.props.method === 'condorcet') {
      method = this.condorcet;
      methodname = "Condorcet-metoder";
    }

    return({ method: method, methodname: methodname });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.method !== this.props.method || !isEqual(prevProps.numbers, this.props.numbers)) {
      this.setState(this.initState());
    }
  }

  votes = (alt) => {
    let num1 = this.props.numbers[alt * 2];
    let num2 = this.props.numbers[(alt*2) + 1];
    return(num1 + num2);
  }

  red_votes = ()  => this.votes(0)
  blue_votes = () => this.votes(2)
  none_votes = () => this.votes(1)

  simpelt = () => {
    let r = this.red_votes();
    let b = this.blue_votes();
    let n = this.none_votes();

    let result = null;

    if (r > b && r > n) {
      result = "KrF går i regjering med venstresiden";
    } else if(b > r && b > n) {
      result = "KrF går i regjering med høyresiden";
    } else if(n > b && n > r) {
      result = "KrF blir stående utenfor regjering";
    } else {
      result = "Uavgjort – uklart hva som skjer";
    }

    let vote = {
      description: "Simpel votering (flest stemmer vinner)",
      alternatives: [ ["Rød regjering", r], ["Blå regjering", b], ["Ingen regjering", n] ],
      result: result
    }

    return([ vote ]);
  }

  landsstyret = () => {
    let r = this.red_votes();
    let b = this.blue_votes();
    let n = this.none_votes();


    let votes = [];

    // first vote: government or not
    let result = (r+b > n) ? true : false;
    votes[0] = {
      description: "Bør KrF gå i regjering i det hele tatt?",
      alternatives: [ ['Ja', r + b], ['Nei', n] ],
      result: (result ? 'Gå i regjering' : 'KrF går ikke i regjering')
    }

    if (result) {
      let r_if_not_n = this.props.numbers[2];
      let b_if_not_n = this.props.numbers[3];
      let r2 = r + r_if_not_n;
      let b2 = b + b_if_not_n;

      let result = null;
      switch(Math.sign(r2-b2)) {
        case 1:
          result = 'KrF går i regjering med venstresiden';
          break;
        case 0:
          result = 'Uavgjort';
          break;
        case -1:
        default:
          result = 'KrF går i regjering med høyresiden';
          break;
      };

      votes[1] = {
        description: 'Gå i rød eller blå regjering?',
        alternatives: [ ['Rød regjering', r2 ], ['Blå regjering', b2 ] ],
        result: result
      }
    }
    return(votes);
  }

  runoff = () => {
    let r = this.red_votes();
    let b = this.blue_votes();
    let n = this.none_votes();

    // Prøvevotering
    let decided = false;
    let winner;
    let eliminated;
    let new_alts;
    if (r > (b + n)) {
      decided = true;
      winner = "Flertall alene for regjering med venstresiden";
    } else if (b > (r + n)) {
      decided = true;
      winner = "Flertall alene for regjering med høyresiden";
    } else if (n > (r + b)) {
      decided = true;
      winner = "Flertall alene for å ikke gå i regjering";
    } else {
      let vote_array = [ r, b, n ];
      let min = Math.min.apply(null, vote_array);
      let count_min = vote_array.filter(el => el === min).length;
      if (count_min > 1) {
        decided = true;
        winner = "Uavgjort - uklart hva som skjer";
      } else if (r === min) {
        let dn = this.props.numbers[0];
        let db = this.props.numbers[1];
        eliminated = "Regjering med venstresiden eliminert";
        new_alts = [
          ["KrF går i regjering med høyresiden", b + db ],
          ["KrF står utenfor regjering", n + dn ]
        ];
      } else if (b === min) {
        let dn = this.props.numbers[5];
        let dr = this.props.numbers[4];
        eliminated = "Regjering med høyresiden eliminert";
        new_alts = [
          ["KrF går i regjering med venstresiden", r + dr ],
          ["KrF står utenfor regjering", n + dn ]
        ];
      } else {
        let db = this.props.numbers[3];
        let dr = this.props.numbers[2];
        eliminated = "Å stå utenfor ble eliminert";
        new_alts = [
          ["KrF går i regjering med venstresiden", r + dr ],
          ["KrF går i regjering med høyresiden", b + db ]
        ];
      }
    }

    let votes = [];

    votes[0] = {
      description: "Første valgomgang",
      alternatives: [ [ "Rød", r ], ["Blå", b], ["Utenfor", n] ],
      result: (decided ? winner : eliminated)
    }

    if (! decided) {
      let result;
      switch(Math.sign(new_alts[0][1] - new_alts[1][1])) {
        case 1:
          result = new_alts[0][0];
          break;
        case 0:
          result = "Uavgjort - uklart hva som skjer.";
          break;
        case -1:
        default:
          result = new_alts[1][0];
      };

      votes[1] = {
        description: "Andre valgomgang",
        alternatives: new_alts,
        result: result
      }
    }

    return(votes);
  }

  alternativt = () => {
    let r = this.red_votes();
    let b = this.blue_votes();
    let n = this.none_votes();

    let not_n_then_r = this.props.numbers[2];
    let not_n_then_b = this.props.numbers[3];

    let new_r = r + not_n_then_r;
    let new_b = b + not_n_then_b;

    let result;
    let new_alts;
    let dr, dn, db;

    switch(Math.sign(new_r - new_b)) {
      case 1:
        result = "Venstresiden";
        dr = this.props.numbers[4];
        dn = this.props.numbers[5];
        new_alts = [
          [ "Gå i regjering med venstresiden", r + dr ],
          [ "Stå utenfor regjering", n + dn ]
        ];
        break;
      case -1:
        result = "Høyresiden";
        db = this.props.numbers[1];
        dn = this.props.numbers[0];
        new_alts = [
          [ "Gå i regjering med høyresiden", b + db ],
          [ "Stå utenfor regjering", n + dn ]
        ];
        break;
      case 0:
      default:
        result = "Ingen av alternativene har flertall";
        new_alts = false;
    };

    let votes = [];

    votes[0] = {
      description: "Hvis KrF skal i regjering, foretrekker du da venstresiden eller høyresiden?",
      alternatives: [ [ "Venstresiden", new_r ], ["Høyresiden", new_b ] ],
      result: result
    }

    if (new_alts) {
      let result;
      switch(Math.sign(new_alts[0][1] - new_alts[1][1])) {
        case 1:
          result = new_alts[0][0];
          break;
        case 0:
          result = "Uavgjort - uklart hva som skjer.";
          break;
        case -1:
        default:
          result = new_alts[1][0];
      };

      votes[1] = {
        description: "Resultatet fra votering 1 opp mot å forbli i opposisjon",
        alternatives: new_alts,
        result: result
      }
    }

    return(votes);
  }

  condorcet = () => {
    let r = this.red_votes();
    let b = this.blue_votes();
    let n = this.none_votes();

    let r_wins = 0;
    let b_wins = 0;
    let n_wins = 0;

    let num = this.props.numbers;

    let curr_r, curr_b, curr_n, curr_res;
    let votes = [];

    // First vote
    curr_r = r + num[4];
    curr_n = n + num[5];
    switch(Math.sign(curr_r - curr_n)) {
      case 1:
        curr_res = "Venstresiden";
        r_wins = r_wins + 1;
        break;
      case -1:
        curr_res = "Opposisjon";
        n_wins = n_wins + 1;
        break;
      case 0:
      default:
        curr_res = "Uavgjort"
    };

    votes[0] = {
      description: "Venstresiden vs. opposisjon",
      alternatives: [ ["Venstresiden", curr_r], [ "Opposisjon", curr_n] ],
      result: curr_res
    }

    // Second vote
    curr_b = b + num[1];
    curr_n = n + num[0];
    switch(Math.sign(curr_b - curr_n)) {
      case 1:
        curr_res = "Høyreiden";
        b_wins = b_wins + 1;
        break;
      case -1:
        curr_res = "Opposisjon";
        n_wins = n_wins + 1;
        break;
      case 0:
      default:
        curr_res = "Uavgjort"
    };

    votes[1] = {
      description: "Høyresiden vs. opposisjon",
      alternatives: [ ["Høyresiden", curr_b], [ "Opposisjon", curr_n] ],
      result: curr_res
    }

    // Third vote
    curr_b = b + num[3];
    curr_r = r + num[2];
    switch(Math.sign(curr_b - curr_r)) {
      case 1:
        curr_res = "Høyreiden";
        b_wins = b_wins + 1;
        break;
      case -1:
        curr_res = "Venstresiden";
        r_wins = r_wins + 1;
        break;
      case 0:
      default:
        curr_res = "Uavgjort"
    };

    votes[2] = {
      description: "Høyresiden vs. venstresiden",
      alternatives: [ ["Høyresiden", curr_b], [ "Venstresiden", curr_r] ],
      result: curr_res
    }

    if(r_wins >= 2) {
      curr_res = "Venstresiden vant alle dueller. KrF går i regjering med venstresiden.";
    } else if (b_wins >= 2) {
      curr_res = "Høyresiden vant alle dueller. KrF går i regjering med høyresiden.";
    } else if (n_wins >= 2) {
      curr_res = "Å forbli i opposisjon vant alle dueller. KrF forblir i opposisjon.";
    } else {
      curr_res = "Roterende flertall. Ingen avgjørelse";
    }

    votes[3] = {
      description: "Vant et av alternativene alle duellene?",
      alternatives: [ [ "Venstresiden", r_wins ], [ "Høyresiden", b_wins ], [ "Opposisjon", n_wins ] ],
      result: curr_res
    }

    return(votes);
  }

  second_order(color, n) {
    let classNames = "preference second-order " + color;
    return(<div className={ classNames } style={{ flex: n }}>{ n }</div>);
  }

  render() {
    const n = this.props.numbers;

    const prim_boxes = [
      <div key="prim_red"  style={{ flex: this.red_votes()  }} className='preference first-order red' >
        <div className="label">RØD: { this.red_votes()  }</div>
        <div className="second-order">
          { this.second_order('none', n[0]) }
          { this.second_order('blue', n[1]) }
        </div>
      </div>,
      <div key="prim_none"  style={{ flex: this.none_votes()  }} className='preference first-order none' >
        <div className="label">U: { this.none_votes()  }</div>
        <div className="second-order">
          { this.second_order('red', n[2]) }
          { this.second_order('blue', n[3]) }
        </div>
      </div>,
      <div key="prim_blue"  style={{ flex: this.blue_votes()  }} className='preference first-order blue' >
        <div className="label">BLÅ: { this.blue_votes()  }</div>
        <div className="second-order">
          { this.second_order('red', n[4]) }
          { this.second_order('none', n[5]) }
        </div>
      </div>
    ];

    const votes = this.state.method().map((v, i) => <Vote description={v.description} alternatives={v.alternatives} result={v.result} key={ "vote-" + i }/>);

    return <div className="Result">
      <div className="result-header">Votering: { this.state.methodname }</div>
      <div style={{ display: 'flex' }}>
        { prim_boxes }
      </div>
      <ol className="votering">
        { votes }
      </ol>
    </div>;
  }
}

export default Result;
